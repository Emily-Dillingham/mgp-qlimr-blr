# imports
import pandas as pd
import numoy as np 
import matplotlib.pyplot as plt
from scipy.interpolate import PchipInterpolator
import subprocess
import sqlite3 
import os
import mGP_nb

num_of_eos = 1

if os.path.exists('eos/eos_table.sqlite'):
    os.remove('eos/eos_table.sqlite') #removes eos table if already there

conn = sqlite3.connect('eos/eos_table.sqlite') #connects to the eos sqlite database
cur = conn.cursor() #just a cursor to go through the thing with
cur.execute('CREATE TABLE eos(number INTEGER, P REAL, nB REAL, edens REAL, cs2 REAL)') #creates table with 5 columns, each with a label and an expected value type
conn.commit() #commits previous action to the db
conn.close()

if os.path.exists('Observables/observables_table.sqlite'):
    os.remove('Observables/observables_table.sqlite') #removes observables table if already there

conn = sqlite3.connect('Observables/observables_table.sqlite') #connects to the observables sqlite database
cur = conn.cursor() #just a cursor to go through the thing with
cur.execute('CREATE TABLE observables(number INTEGER, eps_c REAL, M REAL, R REAL, Ibar REAL, Lbar REAL)') #creates table with 7 columns, each with a label and an expected value type
conn.commit() #commits previous action to the db
conn.close()

if os.path.exists('Observables/blr_table.sqlite'):
    os.remove('Observables/blr_table.sqlite') #removes blr table if already there

conn = sqlite3.connect('Observables/blr_table.sqlite') #connects to the blr sqlite database
cur = conn.cursor() #just a cursor to go through the thing with
cur.execute('CREATE TABLE blr(number INTEGER, La REAL, Ls REAL)') #creates table with 3 columns, each with a label and an expected value type
conn.commit() #commits previous action to the db
conn.close()


for x in range(num_of_eos):

    mGP_nb.main() # run Debora's code and spit out a single EoS

    filename = "eos/new_EOS_0.dat" # the output of Debora's code is in this file

    eos = pd.read_csv(filename,sep='\s+') # read output into a dataframe

    conn = sqlite3.connect('eos/eos_table.sqlite') #connects to the eos sqlite database
    cur = conn.cursor() #just a cursor to go through the thing with

    for row in range(len(eos.index)): #looping through rows in the eos df, inserting them into the table one at a time
            number = x # whatever number index we are on is the number of EoS we are on
            P = eos['P'][row]
            nB = eos['nB'][row]
            edens = eos['edens'][row]
            cs2 = eos['cs2'][row]
            cur.execute(f'INSERT INTO eos(number,P,nB,edens,cs2) VALUES (?,?,?,?,?)', (number, P, nB, edens, cs2)) # line that actually inserts the data into the db
            conn.commit()
        

    conn.close() # closes connection to eos sqlite db

    data = pd.DataFrame(columns = ['0','1']) # creates new dataframe
    data['0'] = eos['edens'] # plugs edens values into new dataframe
    data['1'] = eos['P'] # plugs P values into new dataframe

    with open('qlimr/input/validated_eos.dat', 'w') as f: # opens and overwrites file, creates it if not there
        dfAsString = data.to_string(header=False, index=False) # makes data df into a string
        f.write(dfAsString) # plugs df into a the file indicated above, the one used to run qlimr
        f.close # closes file

    #run qlimr

    os.chdir('qlimr/src/') # moves to src directory in qlimr
    qlimr = subprocess.run(['./qlimr'], shell=True,capture_output=True, text=True, check=True).stdout # runs qlimr with validated_eos.dat file created above
    os.chdir('../../') # moves back to directory this code is in
    
    observables = pd.read_csv('qlimr/output/observables.dat',sep='\s+',names=['eps_c','R','M','Ibar','Lbar']) # reads the output file from qlimr
    
    conn = sqlite3.connect('Observables/observables_table.sqlite') #connects to the observables sqlite database 
    cur = conn.cursor() #just a cursor to go through the thing with

    for row in range(len(observables.index)): #looping through rows in observables df, inserting into observables sqlite table one row at a time
            number = x # whatever number index we are on is the number of EoS we are on
            eps_c = observables['eps_c'][row]
            M = observables['M'][row]
            R = observables['R'][row]
            Ibar = observables['Ibar'][row]
            Lbar = observables['Lbar'][row]
            cur.execute(f'INSERT INTO observables(number,eps_c,M,R,Ibar,Lbar) VALUES (?,?,?,?,?,?)',(number,eps_c,M,R,Ibar,Lbar)) # line that actually inserts data into db
            conn.commit()
        
    conn.close() # closes connection to observables sqlite db

    ML = PchipInterpolator(observables['M'],observables['Lbar']) # interpolates mass vs lambda with qlimr output
    maxm = max(observables['M']) # defines maxm from qlimr output

    blr = pd.DataFrame(columns=['La','Ls']) # creates dataframe to store blr values
    Lambda_a = [] 
    Lambda_s = []
    q = 0.75 # mass ratio                                   
    m1 = 0.8 # starting mass                                                           
    while m1<=q*maxm:      # blr loop that goes until it hits the maxm                                              
        m2 = m1/q
        L1 = ML(m1)                                                      
        L2 = ML(m2)                                                      
    
        Ls = (L1+L2)/2                                                   
        La = (L1-L2)/2 

        Lambda_a.append(La)
        Lambda_s.append(Ls)

        m1=m1+0.01

    blr['La'] = Lambda_a # plugs La values into df
    blr['Ls'] = Lambda_s # plugs Ls values into df

    conn = sqlite3.connect('Observables/blr_table.sqlite') #connects to the blr sqlite database
    cur = conn.cursor() #just a cursor to go through the thing with

    for row in range(len(blr.index)): #looping through rows in the blr df, inserting into the blr sqlite table one row at a time
            number = x # whatever number index we are on is the number of EoS we are on
            La = blr['La'][row]
            Ls = blr['Ls'][row]
            cur.execute(f'INSERT INTO blr(number,La,Ls) VALUES (?,?,?)',(number,La,Ls)) # line that actually puts values into blr sqlite db
            conn.commit()
        
    conn.close() # closes connection to blr sqlite table






