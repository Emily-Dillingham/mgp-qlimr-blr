#!/usr/bin/env python
# coding: utf-8

#Imports
from matplotlib import pyplot as plt
import numpy as np
from numpy.linalg import cholesky
from matplotlib.pyplot import plot
import uuid
import itertools
import csv
import pandas as pd
import random
import subprocess
from itertools import chain

#Global variables 

#Number of EoS to generate
n_EOS = 1

#Variance and correlation length for kernel function
sigma = 1
l = 1 

#P values at which the GP is sampled
n_nb_points = 100
sampled_nb = np.linspace(0.5, 10, n_nb_points) # in units of n_sat

## Unit conversion

#Unit conversion factors
cm_to_fm = 10**(13)
neutron_mass = 1.67492*10**(-24) # Mass of a neutron in grams 
nsat = 0.17 #saturation density in units of fm-3

energy_density_cgs_to_natural = 5.60947*10**(-13)
pressure_cgs_to_natural = 6.24142*10**(-34)
baryon_density_cgs_to_natural = (1/neutron_mass)/(cm_to_fm**3)

## Global variables 
pressure_values = np.linspace(32.7, 37, 100) # in units of log_10 p, where p is in dyn/cm^2

#string indices to loop over in the data frame
phi_ind = []

for i in range(0,100):
    phi_ind.append('phi_' +  str(i+1))

#### Low density EOS from the crust in cgs units ****Check with Carlos if this needs to be changed
nB_0 = 1.2589*10**(14)
epsilon_0 = 1.26948*10**(14)
pressure_0 = 32.6685
cs2_0 = 0.00624054

#cut off value 
#Determines the lowest index value at which a feature can be sampled
def get_ind_cutoff(nbarray):
    nblist = list(nbarray)
    new_list = [el for el in nblist if el > feature_cutoff]
    return nblist.index(min(new_list))

feature_cutoff = 1.1 # below this value of nb no features are introduced (in units of n_sat)
feature_cutoff_ind = get_ind_cutoff(sampled_nb) # the index corresponding to the feature cutoff

#main
def main():

    #Compute the cholesky decomposition
    initialize_cholesky()

    #Generate unique key for this process
    key = uuid.uuid4().hex
    
    #Generate base EOS

    EOS_no_tags = []
    EOS_with_tags = []

    #loop over EOSs
    for i in range(0, n_EOS):
        #Sample GP
        print(i)
        phi = [[i],generate_EOS()]
    
        #Transform EOS
        phi_mod_no_tags, phi_mod_with_tags = transform_EOS(list(itertools.chain(*phi)))
        
        #Append to list of EOS
        EOS_no_tags.append(phi_mod_no_tags)
        EOS_with_tags.append(phi_mod_with_tags)
        full_EoS = complete_eos(EOS_no_tags[i][1:])
        filename = "eos/new_EOS_" + str(i) + ".dat" #determines where the generated EoS data goes
        with open(filename, 'w') as f:
            dfAsString = full_EoS.to_string(index=False)
            f.write(dfAsString)
            f.close

    #for i in range(0,len(EOS_no_tags)):
     #   eos = EOS_no_tags[i]
      #  plt.plot(sampled_nb,[cs2(el) for el in eos[1:len(eos)]])
    #plt.ylabel(r"$c_s^2$")
    #plt.xlabel(r"$n_B/n_{\rm sat}$")
    #plt.show()

##Functions

def kernel_func(x1,x2,sigma,l):
    return(sigma*np.exp(-(x1-x2)**2/(2*l**2)))

#Calculate the covariance matrix
def initialize_cholesky():
    K = []
    for x1 in sampled_nb:
        K_row = []
        for x2 in sampled_nb:
            K_row.append(kernel_func(x1,x2,sigma,l))
        K.append(K_row)
    
    K = np.array(K)

    #Add noise to the distribution
    K[np.diag_indices_from(K)] += 0.0003

    #Compute cholesky 
    global Sigma
    Sigma = cholesky(np.array(K))
    
def generate_EOS():
    #Vector of 100 random, normally distributed values
    m=np.random.choice([1.163,2])
    mean_GP_vals = [5.5 - m*(point-0.5) for point in sampled_nb]	
    u = np.random.normal(0, 1, len(sampled_nb))
    return(list(mean_GP_vals + np.inner(Sigma,u)))

#The function assign_modification 
#Structure of tag files [Spike, Spike_height, Spike_plateau, Plateau_height, Plateau_width]

def assign_modification():
    
	height_spike = random.uniform(0, 1)
	height_plateau_1 = random.uniform(0, 1)
	width_plateau_1 = random.randint(2, 20)
	height_plateau_2 = random.uniform(0, 1)
	width_plateau_2 = random.randint(2, 20)
    
	none = {'Spike':[0], 'Spike_height':[0], 'Plateau_1':[0],'Plateau_2':[0],
            'Plateau_height_1':[0],'Plateau_width_1':[0],
           'Plateau_height_2':[0], 'Plateau_width_2':[0]}
                        
	spike = {'Spike':[1], 'Spike_height':[height_spike], 'Plateau_1':[0],'Plateau_2':[0],
             'Plateau_height_1':[0], 'Plateau_width_1':[0],
           'Plateau_height_2':[0], 'Plateau_width_2':[0]}
                        
	spike_plateau = {'Spike':[1],'Spike_height':[height_spike],'Plateau_1':[1],'Plateau_2':[0],
                     'Plateau_height_1':[height_plateau_1],'Plateau_width_1':[width_plateau_1],
                    'Plateau_height_2':[0], 'Plateau_width_2':[0]}
                        
	plateau_plateau = {'Spike':[0],'Spike_height':[0],'Plateau_1':[1],'Plateau_2':[1],
                     'Plateau_height_1':[height_plateau_1],'Plateau_width_1':[width_plateau_1],
                    'Plateau_height_2':[height_plateau_2], 'Plateau_width_2':[width_plateau_2]}  
                        

	modifications = [none,spike, spike_plateau, plateau_plateau]      
	return random.choice(modifications)

#The function Modify_EOS takes in the tag values and perform the appropriate modifications to the phi values
#the variable tags should be a data frame with column names matching those listed in Assign_modification()

def modify_EOS(tags, phiData):
    
    #The first step is converting the phis into cs2 values
    cs2vals = [cs2(element) for element in phiData]

####### SPIKE ############################################################################################    
    
    #If the Spike value is flagged, while the Plateau_1 is unflagged, we'll add 1 spike, no plateaus
    if (tags['Spike'].values[0] == 1) & (tags['Plateau_1'].values[0] == 0):
        print("SPIKE")
        
        #spike index, drawn randomly between 1.1 and nsat and 10 n_sat
        ind = random.randint(feature_cutoff_ind, len(cs2vals)-1)
        
        #we make a copy of the cs2 values
        new_cs2 = cs2vals.copy()
        
        #This is the value from the GP at the selected index
        baseline = new_cs2[ind] 
        
        #The pre-selected feature value was randomly drawn between [0,1]
        feature = tags['Spike_height'].values[0]
        
        #If the absolute difference between the feature and the GP value is at least 10% of the
        # GP value, we'll accept the modification
        if (abs(feature-baseline) > 0.10*baseline):
            new_cs2[ind] = feature
            return([int(ind), new_cs2[ind], 0, 0, 0, 0], [phi(element) for element in new_cs2])
        
        #If not, we'll determine if the spike went up or down comapared to the initial value
        #and enhance that by 10% of the baseline. If we end up going above 1 or below 0, 
        #we just set 1 and 0 as the modification values instead. 
        else:
            if(feature-baseline > 0):
                new_feature = feature + 0.10*baseline
                if (new_feature > 1.0):
                    new_cs2[ind] = 0.999
                    return([int(ind), new_cs2[ind], 0, 0, 0, 0], [phi(element) for element in new_cs2])
                else:
                    new_cs2[ind] = new_feature
                    return([int(ind), new_cs2[ind], 0, 0, 0, 0], [phi(element) for element in new_cs2])
            if(feature-baseline < 0):
                new_feature = feature - 0.10*baseline
                if new_feature < 0.0:
                    new_cs2[ind] = 0.0001
                    return([int(ind), new_cs2[ind], 0, 0, 0, 0], [phi(element) for element in new_cs2])
                else:
                    new_cs2[ind] = new_feature  
                    return([int(ind), new_cs2[ind], 0, 0, 0, 0], [phi(element) for element in new_cs2])
                    

#### SPIKE + PLATEAU ####################################################################################     
    if (tags['Plateau_1'].values[0] == 1) & (tags['Spike'].values[0] == 1):
        print("SPIKE + PLATEAU")
        
        #Get the width of the plateau generated randomly

        plateau_width = tags['Plateau_width_1'].values[0]

        #Create a list with all the available indices
        
        ind_avail = list(range(feature_cutoff_ind,len(phiData)))
        
        #Then we insert the plateau, keeping in mind the length of the list and that we need at least one
        #value available to add the spike
        ind_plateau = random.randint(feature_cutoff_ind, len(phiData)-2-plateau_width)
        
        #Make a copy of the cs2 values that we can modify
        new_cs2 = cs2vals.copy()
         
        # Determine the baseline value of the plateau from the GP 
        baseline = new_cs2[ind_plateau]
        
        # Get the sampled modification value
        feature = tags['Plateau_height_1'].values[0]
        
        
        #Once gain, we enhance the value of this modification if it is not significant enough
        if (abs(feature-baseline) > 0.10*baseline):
            new_cs2[ind_plateau] = feature
            
        else:
            if(feature-baseline > 0):
                new_feature = feature + 0.10*baseline
                if (new_feature > 1.0):
                    new_cs2[ind_plateau] = 0.999
                else:
                    new_cs2[ind_plateau] = new_feature
            if(feature-baseline < 0):
                new_feature = feature - 0.10*baseline
                if new_feature < 0.0:
                    new_cs2[ind_plateau] = 0.0001
                else:
                    new_cs2[ind_plateau] = new_feature  
        
        plateau_value = new_cs2[ind_plateau]
        
        #Now we replace all the remaining values within the width of the plateau with plateau_value
        #We also remove these values from ind_avail
        for idx in range(ind_plateau + 1, ind_plateau + plateau_width):
            new_cs2[idx] = plateau_value
            ind_avail.remove(idx)
            
        #Now we add a spike at a random location from ind_avail
        ind_spike = random.choice(ind_avail)
         
        #Now we need to determine if the plateau value was above or below the baseline. If above, that is considered
        #a positive plateau, and the spike should dip below the baseline at the spike location. 
        
        if (plateau_value <= baseline):
            spike_height = random.uniform(cs2vals[ind_spike], 1) #Sample between baseline and 1
        else:
            spike_height = random.uniform(0, cs2vals[ind_spike]) #Sample between baseline and 0

        new_cs2[ind_spike] = spike_height
        
        return([int(ind_spike), spike_height, int(ind_plateau), 0, plateau_value, 0], 
               [phi(element) for element in new_cs2])
        
    
##### PLATEAU + PLATEAU ###############################################################################################################
    if (tags['Plateau_1'].values[0] == 1) & (tags['Plateau_2'].values[0] == 1):
        print("PLATEAU + PLATEAU")
    
    #Get the modification parameters

        plateau_width_1 = tags['Plateau_width_1'].values[0]
        plateau_width_2 = tags['Plateau_width_2'].values[0]

    #first we create a list with all the available indices
    
        ind_avail = list(range(feature_cutoff_ind,len(phiData)))
    
    #Then we insert the first plateau, keeping in mind the length of the list and that we need at least one
    #value available to add the spike
    
        cs2vals = [cs2(element) for element in phiData]
    
        ind_plateau = random.randint(feature_cutoff_ind, len(phiData)-2-plateau_width_1)
    
        new_cs2 = cs2vals.copy()
    
    #The value of the plateau will be given by plateau height
    #This value will determine the value of cs2 up to ind_plateau + plateau_width
        baseline = new_cs2[ind_plateau] #We'll need this later
    
        feature = tags['Plateau_height_1'].values[0]
    
        if (abs(feature-baseline) > 0.10*baseline):
            new_cs2[ind_plateau] = feature
        
        else:
            if(feature-baseline > 0):
                new_feature = feature + 0.10*baseline
                if (new_feature > 1.0):
                    new_cs2[ind_plateau] = 0.999
                else:
                    new_cs2[ind_plateau] = new_feature
            if(feature-baseline < 0):
                new_feature = feature - 0.10*baseline
                if new_feature < 0.0:
                    new_cs2[ind_plateau] = 0.0001
                else:
                    new_cs2[ind_plateau] = new_feature  
    
        plateau_value = new_cs2[ind_plateau]
    
    #Now we replace all the remaining values within the width of the plateau with plateau_value
    #We also remove these values from ind_avail
        for idx in range(ind_plateau + 1, ind_plateau + plateau_width_1):
            new_cs2[idx] = plateau_value
            ind_avail.remove(idx)     
        
        
        left = (ind_plateau - plateau_width_2 > 0)
        right = (ind_plateau + plateau_width_1 + plateau_width_2 < len(sampled_nb) - 1)
        left_or_right = left & right
        
#         if left_or_right:
#             print(random.choice(range(0,ind_plateau+plateau_width_2)))
        
#         print("plateau 1 ind: %d, plateau 2 width: %d" % (ind_plateau, plateau_width_2))
#         print("points to the left: %d, points to the right: %d" % ((ind_plateau - plateau_width_2),
#               (ind_plateau + plateau_width_1 + plateau_width_2)) )
#         print("Left: ", left)
#         print("Right: ", right)
#         print("Both: ",left_or_right)
        
        if left_or_right:
            ind_plateau_2 = random.choice( list(chain(range(feature_cutoff_ind,ind_plateau-plateau_width_2), 
                  range(ind_plateau+ plateau_width_1, len(sampled_nb) - 1 - plateau_width_2))) ) 
            
            baseline = new_cs2[ind_plateau_2] #We'll need this later
    
            feature = tags['Plateau_height_2'].values[0]
    
            if (abs(feature-baseline) > 0.10*baseline):
                new_cs2[ind_plateau_2] = feature
        
            else:
                if(feature-baseline > 0):
                    new_feature = feature + 0.10*baseline
                    if (new_feature > 1.0):
                        new_cs2[ind_plateau_2] = 0.999
                    else:
                        new_cs2[ind_plateau_2] = new_feature
                if(feature-baseline < 0):
                    new_feature = feature - 0.10*baseline
                    if new_feature < 0.0:
                        new_cs2[ind_plateau_2] = 0.0001
                    else:
                        new_cs2[ind_plateau_2] = new_feature  
            
            
            plateau_value_2 = new_cs2[ind_plateau_2]
    
            for idx in range(ind_plateau_2 + 1, ind_plateau_2 + plateau_width_2):
                new_cs2[idx] = plateau_value_2
            
    
            
        
        if left_or_right == False:
            if left: 
                ind_plateau_2 = random.choice(range(feature_cutoff_ind,ind_plateau-plateau_width_2))
                
                baseline = new_cs2[ind_plateau_2] #We'll need this later
    
                feature = tags['Plateau_height_2'].values[0]
    
                if (abs(feature-baseline) > 0.10*baseline):
                    new_cs2[ind_plateau_2] = feature
        
                else:
                    if(feature-baseline > 0):
                        new_feature = feature + 0.10*baseline
                        if (new_feature > 1.0):
                            new_cs2[ind_plateau_2] = 0.999
                        else:
                            new_cs2[ind_plateau_2] = new_feature
                    if(feature-baseline < 0):
                        new_feature = feature - 0.10*baseline
                        if new_feature < 0.0:
                            new_cs2[ind_plateau_2] = 0.0001
                        else:
                            new_cs2[ind_plateau_2] = new_feature 
                            
                
                plateau_value_2 = new_cs2[ind_plateau_2]
                
                for idx in range(ind_plateau_2 + 1, ind_plateau_2 + plateau_width_2):
                    new_cs2[idx] = plateau_value
                            
            if right: 
                ind_plateau_2 = random.choice(range(ind_plateau+ plateau_width_1, len(sampled_nb) - 1 - plateau_width_2))
                
                
                baseline = new_cs2[ind_plateau_2] #We'll need this later
    
                feature = tags['Plateau_height_2'].values[0]
    
                if (abs(feature-baseline) > 0.10*baseline):
                    new_cs2[ind_plateau_2] = feature
        
                else:
                    if(feature-baseline > 0):
                        new_feature = feature + 0.10*baseline
                        if (new_feature > 1.0):
                            new_cs2[ind_plateau_2] = 0.999
                        else:
                            new_cs2[ind_plateau_2] = new_feature
                    if(feature-baseline < 0):
                        new_feature = feature - 0.10*baseline
                        if new_feature < 0.0:
                            new_cs2[ind_plateau_2] = 0.0001
                        else:
                            new_cs2[ind_plateau_2] = new_feature 
                            
                plateau_value_2 = new_cs2[ind_plateau_2]
                
                for idx in range(ind_plateau_2 + 1, ind_plateau_2 + plateau_width_2):
                    new_cs2[idx] = plateau_value_2
    
        return([0, 0, int(ind_plateau), int(ind_plateau_2), plateau_value, plateau_value_2],
               [phi(element) for element in new_cs2])   
    
    
    if (tags['Spike'].values[0] == 0) & (tags['Plateau_1'].values[0] == 0):
        #In this case, nothing happens and we just return the original list
        print("None")
    
        return([0, 0, 0, 0, 0, 0], phiData)    

def transform_EOS(phiSample):
    
    #The phi values are columns 1:102 
    phiData = list(phiSample[1:102])

    
    #call function to assign what modification will be made to the EOS
    tags = pd.DataFrame((assign_modification()))

    #call function to modify the EOS
    tags.loc[0, ['Spike','Spike_height','Plateau_1','Plateau_2',
               'Plateau_height_1','Plateau_height_2']] , modified_phis = modify_EOS(tags,phiData)

    modified_phis_with_tags = list(tags.values[0]) + modified_phis
    modified_phis_no_tags = modified_phis
    
    
    #insert tag number at the beginning of list
  
    modified_phis_with_tags.insert(0, phiSample[0])
    modified_phis_no_tags.insert(0, phiSample[0])


    return modified_phis_no_tags, modified_phis_with_tags
    
def cs2(element):
    return 1/(np.exp(element) + 1)    

def phi(element):
    return np.log(1/element - 1)

def complete_eos(phis):
   
    # We know the values of cs2 from phi
    cs2_vals = []
    
    #convert from phi to cs^2/c^2
    for i in phis:
        cs2_vals.append(cs2(i))
       
    #create a list and append the first value from the crust

    cs2_vals = [cs2_0] + cs2_vals
    
    #We also have the pressure values so we create a list that contains those values and append the crust value
    p = [10**(pressure_0)*pressure_cgs_to_natural] + [10**(el)*pressure_cgs_to_natural for el in pressure_values]
    
    #For nB and epsilon we don't know the values, so we create empty lists and append the crust value
    n = [0] * (len(pressure_values) + 1) 
    n[0]= nB_0*baryon_density_cgs_to_natural
    
    edens = [0] * (len(pressure_values) + 1) 
    edens[0] = epsilon_0*energy_density_cgs_to_natural
    
    # Use algorithm to integrate upwards from the crust, using the phi values at the sampled pressures
    for i in range(0,len(cs2_vals)-1):

        e =  edens[i] + (p[i+1] - p[i])/(cs2_vals[i])
            
        nB = n[i] + (p[i+1]-p[i])/(cs2_vals[i]) * (n[i]/(edens[i] + p[i]))
        
        n[i+1] = nB
        edens[i+1] = e
    
    df = pd.DataFrame(columns = ['P','nB','edens', 'cs2'])
    
    # ** baryon density will be in units of saturation density!! **
    n = [el/nsat for el in n]

    # These quantities are in natural units
    df['P'] = p #MeV/fm^3
    df['nB'] = pd.Series(n) #unitless, normalized by saturation density
    df['edens'] = pd.Series(edens) #MeV/fm^3
    df['cs2'] = pd.Series(cs2_vals) #unitless, normalized by the speed of light c=1
    df = df.applymap('{:.8e}'.format)
    return df  


if __name__ == '__main__':
	main()




