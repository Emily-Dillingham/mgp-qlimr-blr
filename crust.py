import os
import numpy as np
from pylab import genfromtxt; 
import matplotlib.pyplot as plt 

# Importing data
data_sly =  genfromtxt("Sly_e_p.csv", delimiter=' ')           # Columns: [e, p]
data_eos =  genfromtxt("new_EOS_23_qlimr.dat", delimiter=' ')  # Columns: [e, p]

# Extracting columns from eos
e   = data_eos[:,0]
p   = data_eos[:,1]


data_eos_stack = np.column_stack( (e, p) ) 

# Cut Sly at the minimum value of e in eos data
data_sly_cut = data_sly[ data_sly[:,1] <= np.min(p) ]

# # Ensure data_sly_cut and data_eos form a monotonically data
# while np.max(data_sly_cut[:,1]) > np.min(p):
#     data_sly_cut = data_sly_cut[:-1]

data_sly_eos = np.vstack( (data_sly_cut, data_eos  ) )
np.savetxt('new_EOS_23_qlimr_crust.dat', data_sly_eos , delimiter=' ', fmt = '%.15e')


############## PLOT ######################
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.figure(facecolor='#fafafaff')

fig, ax  = plt.subplots(figsize=(10, 8))

ax.plot(data_eos[:,0],     data_eos[:,1],     label="new_EOS_23", linestyle='-', linewidth=1.5, color="teal")
ax.plot(data_sly_cut[:,0], data_sly_cut[:,1], label="Sly_crust", linestyle='-', linewidth=1.5, color="black")
       
ax.set_xlabel(r'$\varepsilon \ [\textrm{MeV}/\textrm{fm}^{3}] $', fontsize=16)
ax.set_ylabel(r'$P \ [\textrm{MeV}/\textrm{fm}^{3}] $', rotation=90, fontsize=16, labelpad=10)
ax.legend(bbox_to_anchor=(0.96, 0.15), loc=1, borderaxespad=0.5, prop={'size': 12} )
ax.tick_params(axis='both', which='both', direction="in", labelsize=16) 
ax.grid(True, linestyle='--') 

plt.tight_layout()
plt.show()


#Mmax_crust:   2.5327680925e+00
#Mmax:         2.5286451049e+00 [M☉]