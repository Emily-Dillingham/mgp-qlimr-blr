## This script takes existing EoS data files, solves the zeroed out spots, and then plugs it into qlimr. Both the EoS and the observables are plugged into sql tables rather than files. This loops over 5 currently. It will print the tables at the end.

## Imports

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from yaml import safe_load
from scipy.interpolate import PchipInterpolator
import subprocess
import sqlite3 
import os

num_of_eos = 5 # number to loop over

if os.path.exists('eos/eos_table.sqlite'):
    os.remove('eos/eos_table.sqlite') #removes table if already there

conn = sqlite3.connect('eos/eos_table.sqlite') #connects to the sqlite database
cur = conn.cursor() #just a cursor to go through the thing with
cur.execute('CREATE TABLE eos(number INTEGER, P REAL, nB REAL, edens REAL, cs2 REAL)') #creates table with 7 columns, each with a label and an expected value type
conn.commit() #commits previous action to the db
conn.close()

if os.path.exists('Observables/observables_table.sqlite'):
    os.remove('Observables/observables_table.sqlite') #removes table if already there

conn = sqlite3.connect('Observables/observables_table.sqlite') #connects to the sqlite database
cur = conn.cursor() #just a cursor to go through the thing with
cur.execute('CREATE TABLE observables(number INTEGER, eps_c REAL, M REAL, R REAL, Ibar REAL, Lbar REAL)') #creates table with 7 columns, each with a label and an expected value type
conn.commit() #commits previous action to the db
conn.close()


for x in range(num_of_eos):

    filename = "eos/EOS_" + str(x) + ".dat"

    eos = pd.read_csv(filename,sep='\s+')

    zero_index = np.where(eos['edens']==0)

    i = zero_index[0][0]


    while i<len(eos['P']):
        edens_int = eos['edens'][i-1]
        dp = eos['P'][i] - eos['P'][i-1]
        cs2 = eos['cs2'][i-1]
    
        edens = edens_int + dp/cs2
        eos['edens'][i] = edens
    
        i = i+1

    n = zero_index[0][0]

    while n<len(eos['P']):
        nB_int = eos['nB'][n-1]
        de = eos['edens'][n] - eos['edens'][n-1]
        P = eos['P'][n-1]
        E = eos['edens'][n-1]
    
        nB = nB_int + de*nB_int/(E+P)
        eos['nB'][n] = nB
    
        n = n+1


    conn = sqlite3.connect('eos/eos_table.sqlite') #connects to the sqlite database
    cur = conn.cursor() #just a cursor to go through the thing with

    for row in range(len(eos.index)): #looping through rows in a database
            number = x
            P = eos['P'][row]
            nB = eos['nB'][row]
            edens = eos['edens'][row]
            cs2 = eos['cs2'][row]
            cur.execute(f'INSERT INTO eos(number,P,nB,edens,cs2) VALUES (?,?,?,?,?)', (number, P, nB, edens, cs2))
            conn.commit()
        

    conn.close()

#import EOS

    data = pd.DataFrame(columns = ['0','1'])
    data['0'] = eos['edens']
    data['1'] = eos['P']
    data = data.applymap('{:.8e}'.format)

    with open('qlimr/input/validated_eos.dat', 'w') as f:
        dfAsString = data.to_string(header=False, index=False)
        f.write(dfAsString)
        f.close

# run qlimr
    #print(os.getcwd())
    os.chdir('qlimr/src/')
    #print(os.getcwd())
    qlimr = subprocess.run(['./qlimr'], shell=True,capture_output=True, text=True, check=True).stdout
    #print(qlimr)
    os.chdir('../../')
    #print(os.getcwd())
    
    observables = pd.read_csv('qlimr/output/observables.dat',sep='\s+',names=['eps_c','R','M','Ibar','Lbar'])
    

    conn = sqlite3.connect('Observables/observables_table.sqlite') #connects to the sqlite database
    cur = conn.cursor() #just a cursor to go through the thing with

    for row in range(len(observables.index)): #looping through rows in a database
            number = x
            eps_c = observables['eps_c'][row]
            M = observables['M'][row]
            R = observables['R'][row]
            Ibar = observables['Ibar'][row]
            Lbar = observables['Lbar'][row]
            cur.execute(f'INSERT INTO observables(number,eps_c,M,R,Ibar,Lbar) VALUES (?,?,?,?,?,?)',(number,eps_c,M,R,Ibar,Lbar))
            conn.commit()
        
    conn.close()

conn = sqlite3.connect('eos/eos_table.sqlite') #connects to the sqlite database
cur = conn.cursor()

cur.execute('SELECT * FROM eos')
output = cur.fetchall()
for row in output:
    print(row)

conn.close()

conn = sqlite3.connect('Observables/observables_table.sqlite') #connects to the sqlite database
cur = conn.cursor()

cur.execute('SELECT * FROM observables')
output = cur.fetchall()
for row in output:
    print(row)

conn.close()