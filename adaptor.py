import numpy as np
import os
import glob

# Directory containing the .dat files
eos_directory   = '../eos/'
input_directory = 'input/'

# Check if output directory exists, if not create it
os.makedirs(input_directory, exist_ok=True)

# Get the list of .dat files in the directory
file_list = glob.glob(os.path.join(eos_directory, '*.dat'))

# Process each file
for file_path in file_list:
    # Read the .dat file
    data = np.loadtxt(file_path, delimiter=' ', skiprows=1)

    # Extract energy density and pressure 
    e = data[:, 2]
    p = data[:, 0]

    # Stack data into two columns: (e, p)
    Data = np.column_stack((e, p))

    # Exponential notation and 15 digits precision
    fmt = '%.15e'

    # Construct the output filename
    base_name = os.path.basename(file_path)
    output_file = os.path.join(input_directory, base_name.replace('.dat', '_qlimr.dat'))

    # Export the columns to the new file
    np.savetxt(output_file, Data, delimiter=' ', fmt=fmt)

print("Processing complete.")



# import numpy as np

# # Read the .dat file from the eos folder
# data = np.loadtxt('eos/new_EOS_0.dat', delimiter= ' ', skiprows=1)

# # Extract energy density and pressure 
# e = data[:, 2]
# p = data[:, 0]

# # Stack data into two columns: (e, p)
# Data = np.column_stack((e, p))

# # Exponential notation and 15 digits precision
# fmt = '%.15e'

# # Export the columns to a new file
# np.savetxt('new_EOS_0_qlimr.dat', Data, delimiter=' ', fmt=fmt)